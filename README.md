
cd /Users/andrew/Code/Web/standardStoppage/two/assets/video/
ffmpeg -re -i ./01_lindqwister.mp4 -c copy -f flv rtmp://stream.openmix.xyz/live/foxface?md5=V5EHHpuEaLv0aORnpe9DIg&expires=1585706384

sudo service antmedia status

Trouble with ANT:
https://github.com/ant-media/Ant-Media-Server/issues/579


Tutorials:
https://obsproject.com/forum/resources/how-to-set-up-your-own-private-rtmp-server-using-nginx.50/
https://josuesworld.wordpress.com/2019/04/05/how-to-use-rtmp-and-rtmps-with-nginx-on-windows/
https://josuesworld.wordpress.com/2019/04/05/how-to-use-rtmp-and-rtmps-with-nginx-on-windows/


Start:

apt-get install iptables
apt-get install socat
curl https://get.acme.sh | sh

export CF_Token="H5JrVjKZvH1DqFC46ZiH1_x_DhU0wCnMA-EuFXd0"
export CF_Account_ID="dac25219b7c063cc97edeeb3f2caf472"
acme.sh --issue --dns dns_cf -d secretvenue.org -d *.secretvenue.org

rsync -avzh ./iptables/iptables.conf root@stream.openmix.xyz:/etc/iptables.conf
rsync -avzh ./iptables/iptables.service root@stream.openmix.xyz:/etc/systemd/system/
rsync -avzh ./nginx/nginx.service root@stream.openmix.xyz:/etc/systemd/system/

sudo apt-get install iptables-persistent
iptables-restore -n /etc/iptables.conf
sudo systemctl enable iptables
sudo systemctl start iptables
reboot


sudo systemctl enable  nginx
sudo systemctl start  nginx


mkdir /dh-param
sudo openssl dhparam -out /dh-param/dhparam-2048.pem 2048

# Compile Streaming Nginx

sudo apt-get install build-essential libpcre3 libpcre3-dev libssl-dev
wget http://nginx.org/download/nginx-1.9.9.tar.gz
wget https://github.com/sergey-dryabzhinsky/nginx-rtmp-module/archive/dev.zip
tar -zxvf nginx-1.9.9.tar.gz
cd nginx-1.9.9/

sudo apt install zlib1g-dev unzip
sudo apt install libssl1.0-dev
sudo apt install libxml2-dev libxslt1-dev 
./configure --with-http_xslt_module --prefix=/usr/local/nginx --with-http_ssl_module --add-module=../nginx-rtmp-module-dev --with-http_v2_module --with-ipv6 --with-cc-opt="-Wno-error" --with-http_secure_link_module
make
sudo make install
